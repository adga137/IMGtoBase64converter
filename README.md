# IMGaBase64converter

- English:
	Simple converter of local images to Base64 encoding made in HTML and Javascript. 
- Spanish:
	Sencillo Conversor de imagenes locales a base64 hecho en HTML y Javascript.

# Requeriments | Requisitos

- English:
	A web browser that supports Javascript DOM and CSS3.
- Spanish:
	Un navegador web que soporte Javascript DOM y CSS3.


# Licence | Licencia

- English:
	This small project is licensed under GPLv3.
- Spanish:
	Este pequeño proyecto esta bajo licencia GPLv3.

##URL licence:
[https://www.gnu.org/licenses/gpl-3.0.html]
